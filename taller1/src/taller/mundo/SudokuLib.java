package taller.mundo;

/*
 * SudokuLib.java
 * This file is part of SudokuCLI
 *
 * Copyright (C) 2016 - ISIS1206 Team
 *
 * SudokuCLI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SudokuCLI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SudokuCLI. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.List;
import java.util.Random;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Collections;
import org.jblas.DoubleMatrix;

/**
 *  La clase <tt>SudokuLib</tt> contiene procedimientos, utilidades y otras funciones esenciales para
 *  verificar y generar un juego de sudoku. La generación del tablero de juego se basa en el uso de un
 *  método de optimización metaheurística (Simulated Annealing), el cual, a partir de un tablero que 
 *  contiene todos los valores existentes, obtiene un tablero que cumple con las restricciones dispuestas 
 *  por parte de las reglas del juego de Sudoku. El problema de generar un tablero de sudoku, se reduce 
 *  al problema de hallar una solución al problema de <a href="https://en.wikipedia.org/wiki/Graph_coloring">Graph Coloring</a>, 
 *  cuyo <a href="http://mathworld.wolfram.com/ChromaticNumber.html">número cromático<a> es conocido, <i>i.e.,</i> 9.
 *  @author ISIS1206 Team
 */


public class SudokuLib
{
    /**
     * Instancia de un generador pseudo-aleatorio de números.
     **/
    private static Random random = new Random();

    /**
     * Definición de un juego de sudoku como una instancia de un grafo, en el cual,
     * las casillas que pertenecen 
     **/
    private static HashMap<Integer, ArrayList<Integer>> edges;

    /**
     * Dada una coordenada en un tablero de sudoku, verifica si el valor introducido
     * cumple con las restricciones del juego.
     *
     * @param board Matriz que describe el estado actual del juego de Sudoku.
     * Cada entrada de la matriz, contiene un número entre 1 y 9 si ésta no se
     * encuentra vacía, 0 de lo contrario. board != null
     * @param row Número entero que describe la fila en la cual se encuentra la 
     * casilla a inspeccionar. 0 <= row < 9
     * @param col Número entero que describe la columna en la cual se encuentra la 
     * casilla a inspeccionar. 0 <= col < 9
     * @return Una cadena de texto que contiene las casillas (Separadas por coma) 
     * que contienen el mismo número que la casilla sujeta a inspección
     **/
    public static String checkConstraintsEntry(int[][] board, int row, int col)
    {
         String errorBoxes = "";
         if(board[row][col] != 0)
         {
            int num = 9*row+col;
            ArrayList<Integer> outEdges = edges.get(num);
            for(Integer node : outEdges)
            {
                int r = node/9;
                int c = node % 9;
                if(board[r][c] != 0)
                {
                     if(board[row][col] == board[r][c])
                     {
                          errorBoxes += String.format("%c%d, ", ((char) r + 65), c+1);
                     }
                }
            }
         }
         return errorBoxes;
    }

    /**
     * Establece el número de casillas que incumplen las restricciones impuestas
     * por el juego. (Cada ocurrencia de una violación representa un incremento en 2).
     *
     * @param b Matriz que describe el estado actual del juego de Sudoku.
     * Cada entrada de la matriz, contiene un número entre 1 y 9 si ésta no se
     * encuentra vacía, 0 de lo contrario. b != null
     * @return Número de casillas que incumplen las restricciones de juego. 
     * 0 <= num <= 81*2.
     **/
    public static int evalConstraints(int[][] b)
    {
         int error = 0;
         for(Integer node : edges.keySet())
         {
              int r1 = node/9;
              int c1 = node % 9;
              if(b[r1][c1] != 0)
              {
                 for(Integer neighbor : edges.get(node))
                 {
                      int r2 = neighbor/9;
                      int c2 = neighbor % 9;
                      if(b[r1][c1] == b[r2][c2])
                      {
                           error++;
                      }
                 }
              }
              else
              {
                  error++;
              }
         }
         return error;
    }

    /**
     * Establece el número de nodos que incumplen las restricciones impuestas
     * por el problema de Graph Coloring. 
     * (Cada ocurrencia de una violación representa un incremento en 2).
     *
     * @param b Diccionario que representa el juego de Sudoku, cada llave representa 
     * cada una de las casillas existentes en el juego (0 <= casilla <= 81), y el 
     * valor asociado corresponde al número contenido al interior de la misma.
     * @return Número de casillas que incumplen las restricciones de juego. 
     * 0 <= num <= 81*2.
     **/
    private static int evalConstraints(HashMap<Integer, Integer> b)
    {
         int error = 0;
         for(Integer node : edges.keySet())
         {
              for(Integer neighbor : edges.get(node))
              {
                   if(b.get(node) == b.get(neighbor))
                   {
                       error++;
                   }
              }
         }
         return error;
    }

    /**
     * Dado un tablero de Sudoku, intercambia (En múltiples ocasiones) 
     * los valores de dos casillas elegidas de forma pseudo-aleatoria.
     *
     * @param sq Diccionario que representa el juego de Sudoku, cada llave representa 
     * cada una de las casillas existentes en el juego (0 <= casilla <= 81), y el 
     * valor asociado corresponde al número contenido al interior de la misma. sq != null
     * @param times Número de ejecuciones del procedimiento de intercambio de números.
     * 1 <= times <= Integer.MAX_INT
     * @return Diccionario que representa el juego de Sudoku, con algunos de los valores
     * intercambiados de forma pseudo-aleatoria.
     **/
    private static HashMap<Integer, Integer> swapValues(HashMap<Integer, Integer> sq, int times)
    {
          HashMap<Integer, Integer> temp = new HashMap<Integer, Integer>(sq);
          for(int i = 0; i < times; i++)
          {
               while(true)
               {
                    int r1 = random.nextInt(81);
                    int r2 = random.nextInt(81);
                    if(r1 != r2)
                    {
                         int val = temp.get(r1);
                         temp.put(r1, temp.get(r2));
                         temp.put(r2, val);
                         break;
                    }
               }
          }
          return temp;
    }

    /**
     * Genera un tablero de Sudoku que contiene todos los valores existentes
     * en el juego, cuyas casillas podrían no cumplir las restricciones del juego.
     * Adicionalmente, establece una representación del juego de Sudoku como un grafo,
     * bajo el cual, cada casilla es representada como un nodo etiquetado entre 0 y 80,
     * cuyo <i>color</i> corresponde al número contenido en su interior.
     *
     * @return Diccionario que representa el juego de Sudoku, cada llave representa 
     * cada una de las casillas existentes en el juego (0 <= casilla <= 81), y el 
     * valor asociado corresponde al número contenido al interior de la misma.
     */
    private static HashMap<Integer, Integer> initialSquare()
    {
          if(edges == null)
          {
               List<Tuple<Integer, Integer>> l = new ArrayList<Tuple<Integer, Integer>>();
               edges = new HashMap<Integer, ArrayList<Integer>>();
               for(int i = 0; i < 81; i++)
               {
                   edges.put(i, new ArrayList<Integer>());
               }

               for(int i = 0; i < 81; i++)
               {
                    for(int j = 0; j < 9 - (i % 9); j++)
                    {
                        l.add(new Tuple<Integer, Integer>(i, i+j));
                    }
               }

               for(int i = 0; i < 9; i++)
               {
                   ArrayList<Integer> row = new ArrayList<Integer>();
                   for(int j = i; j < 81; j += 9)
                   {
                        row.add(j);
                   }

                   for(int j = 0; j < row.size(); j++)
                   {
                       for(int k = j+1; k < row.size(); k++)
                       {
                            l.add(new Tuple<Integer, Integer>(row.get(j), row.get(k)));
                       }
                   }
               }

               for(int i = 0; i < 9; i += 3)
               {
                   for(int j = 0; j < 9; j += 3)
                   {
                       ArrayList<Integer> e = new ArrayList<Integer>();
                       for(int k = 0; k < 3; k++)
                       {
                           for(int g = 0; g < 3; g++)
                           {
                               e.add(9*(i+k)+j+g);
                           }
                       }
                       for(int k = 0; k < e.size(); k++)
                       {
                           for(int g = k+1; g < e.size(); g++)
                           {
                               l.add(new Tuple<Integer, Integer>(e.get(k), e.get(g)));
                           }
                       }
                   }
               }

               l = new ArrayList<Tuple<Integer, Integer>>(new HashSet<Tuple<Integer, Integer>>(l));
//               l = l.stream().filter(elt -> elt.x != elt.y).collect(Collectors.toList());
               for(Tuple<Integer, Integer> t : l)
               {
                    if(t.x != t.y)
                    {
                        edges.get(t.x).add(t.y);
                        edges.get(t.y).add(t.x);
                    }
               }
          }

          ArrayList<ArrayList<Integer>> b = new ArrayList<ArrayList<Integer>>();
          for(int i = 0; i < 9; i++)
          {
              ArrayList<Integer> row = new ArrayList<Integer>();
              for(int j = 0; j < 9; j++)
              {
                   row.add(j+1);
              }
              Collections.shuffle(row);
              b.add(row);
          }

          HashMap<Integer, Integer> sq = new HashMap<Integer, Integer>();
          for(int i = 0; i < b.size(); i++)
          {
              ArrayList<Integer> row = b.get(i);
              for(int j = 0; j < row.size(); j++)
              {
                   int col = row.get(j);
                   sq.put(9*i+j, col);
              }
          }
          return sq;
    }

    /**
     * Genera una matriz binaria dispersa de dimensión 9x9 que contiene exactamente
     * <i>n</i> entradas que contienen el número uno.
     *
     * @param n El número de unos que se desean almacenar en la matriz. 0 <= n <= 9
     * @return Matriz binaria dispersa de dimensión 9x9 que contiene exactamente
     * <i>n</i> entradas que contienen el número uno.
     **/
    private static DoubleMatrix genMask(int n)
    {
        DoubleMatrix mask = DoubleMatrix.zeros(9, 9);
        for(int i = 0; i < n; i++)
        {
             while(true)
             {
                 int row = random.nextInt(9);
                 int col = random.nextInt(9);
                 if(mask.get(row, col) == 0)
                 {
                     mask.put(row, col, 1);
                     break;
                 }
             }
        }
        return mask;
    }

    private static double[][] int2double(int[][] m)
    {
        double[][] d = new double[m.length][];
        for(int i = 0; i < m.length; i++)
        {
             double[] row = new double[m[i].length];
             for(int j = 0; j < row.length; j++)
             {
                 row[j] = m[i][j];
             }
             d[i] = row;
        }
        return d;
    }

    private static int[][] double2int(double[][] m)
    {
        int[][] d = new int[m.length][];
        for(int i = 0; i < m.length; i++)
        {
             int[] row = new int[m[i].length];
             for(int j = 0; j < row.length; j++)
             {
                 row[j] = (int) m[i][j];
             }
             d[i] = row;
        }
        return d;
    }

    /**
     * Dada una Matriz que representa la solución de un juego de Sudoku,
     * genera un tablero incompleto de sudoku que contiene <i>n</n> ayudas.
     *
     * @param solution Matriz que describe una solución de un juego de Sudoku,
     * cada entrada contiene un número que se encuentra en el rango [1, 9].
     * @param n Número de ayudas que debe contener el tablero de Sudoku generado
     * por parte del sistema. 0 < n <= 9
     * @return Matriz que describe un juego incompleto de Sudoku.
     * Cada entrada de la matriz, contiene un número entre 1 y 9 si ésta no se
     * encuentra vacía, 0 de lo contrario.
     **/
    public static int[][] getSudokuBoard(int[][] solution, int n)
    {
         DoubleMatrix board = new DoubleMatrix(int2double(solution));
         DoubleMatrix mask = genMask(n);
         mask = mask.mul(board);
         return double2int(mask.toArray2());
    }

    /**
     * Genera un tablero aleatorio de Sudoku, como una solución al problema de
     * Graph Coloring, sujeto a un número cromático correspondiente a 9, sobre un
     * grafo que contiene 81 nodos y 1620 arcos. La función se basa en la implementación
     * del método metaheurístico conocido como Simulated Annealing.
     * <p>
     * <b>Para obtener mayor información, consultar:</b>
     * <a href="https://en.wikipedia.org/wiki/Simulated_annealing">Simulated Annealing</a>
     *
     * @param t El tiempo máximo (En milisegundos) de ejecución de la rutina de optimización
     * @param verbose true si se desea imprimir información de ejecución en consola. false, de lo contrario
     * @return Matriz que describe una solución de un juego de Sudoku,
     * cada entrada contiene un número que se encuentra en el rango [1, 9].
     **/
    public static int[][] simulated_annealing(int t, boolean verbose)
    {
        int ref_cost;
        int iter = 0;
        double initial_temp = 1.0;
        int steps = 300;
        double cooling = 0.97;
        double constant = 0.01;
        HashMap<Integer, Integer> sq = initialSquare();
        int cost = evalConstraints(sq);
        double temp = initial_temp;
        boolean valid = false;
        long c_time = System.currentTimeMillis();
        while(!valid)
        {
            if(cost == 0)
            {
                valid = true;
                break;
            }
            temp *= cooling;
            ref_cost = cost;
            for(int i = 0; i < steps; i++)
            {
                HashMap<Integer, Integer> temp_sq = swapValues(sq, 1);
                int n_cost = evalConstraints(temp_sq);
                if(n_cost < cost || Math.exp(-(n_cost-cost)/(temp*constant)) > random.nextDouble())
                {
                     cost = n_cost;
                     sq = temp_sq;
                }
                if(cost == 0)
                {
                     valid = true;
                     break;
                }
            }
            if(ref_cost != cost)
            {
                temp /= cooling;
            }
            if(verbose)
            {
                System.out.printf("Iteration %d | Cost : %d\n", iter, cost);
            }
            iter++;
            if(System.currentTimeMillis()-c_time > t)
            {
                 break;
            }
        }
        int[][] values = new int[9][];
        List<Integer> m = new ArrayList<Integer>();
        //sq.forEach((k, v) -> m.add(v));
        for(Integer k : sq.keySet())
        {
        	m.add(sq.get(k));
        }
        int s = 0;
        for(int i = 0; i < 9; i++)
        {
            int[] row = new int[9];
            for(int j = s; j < s+9; j++)
            {
                row[j-s] = m.get(j);
            }
            s += 9;
            values[i] = row;
        }
        return values;
    }

    private static class Tuple<X, Y>
    {
          public final X x; 
          public final Y y; 
          
          public Tuple(X x, Y y) 
          { 
              this.x = x; 
              this.y = y; 
          }

          @Override
          public String toString() {
              return "(" + x + "," + y + ")";
          }

          @SuppressWarnings("unchecked")
	       @Override
          public boolean equals(Object other) {
              if (other == null) {
                  return false;
              }
              if (other == this) {
                  return true;
              }
              if (!(other instanceof Tuple)){
                  return false;
              }
              Tuple<X,Y> other_ = ((Tuple<X,Y>) other);
              return other_.x == this.x && other_.y == this.y;
          }

          @Override
          public int hashCode() 
          {
              final int prime = 31;
              int result = 1;
              result = prime * result + ((x == null) ? 0 : x.hashCode());
              result = prime * result + ((y == null) ? 0 : y.hashCode());
              return result;
          }

    }

}
