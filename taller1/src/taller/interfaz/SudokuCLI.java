package taller.interfaz;

/*
 * SudokuCLI.java
 * This file is part of SudokuCLI
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * SudokuCLI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SudokuCLI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SudokuCLI. If not, see <http://www.gnu.org/licenses/>.
 */


import taller.mundo.Sudoku;
import static taller.mundo.Sudoku.Rating;

import java.util.Map;
import java.util.Set;
import java.util.Scanner;
import java.util.ArrayList;

/**
 *  La clase <tt>SudokuCLI</tt> establece un sistema de interacción directa
 *  con un usuario final, basada en el uso de un entorno de comando de línea (CLI).
 *  A través de este, un usuario puede acceder y jugar al Sudoku.
 *  @author ISIS1206 Team
 */

public class SudokuCLI
{
	/**
	 * Referencia directa a un juego de Sudoku.
	 **/
	private Sudoku sudoku;

	/**
	 * Interfaz principal al flujo estándar de entrada.
	 * Permite obtener la entrada del usuario a través de consola
	 **/
	private Scanner in;

	/**
	 * Constuctor principal de la clase
	 **/
	public SudokuCLI()
	{
		in = new Scanner(System.in);
		sudoku = new Sudoku();
	}

	/**
	 * Menú principal del juego
	 **/
	public void mainMenu()
	{
		boolean finish = false;
		while(!finish)
		{
			Screen.clear();
			System.out.println("----------------");
			System.out.println("-              -");
			System.out.println("-    Sudoku    -");
			System.out.println("-              -");
			System.out.println("----------------\n");

			System.out.println("Menú principal");
			System.out.println("--------------");
			System.out.println("1. Iniciar nuevo juego");
			System.out.println("2. Salir\n");

			System.out.print("Seleccione una opción: ");
			int opt = in.nextInt();

			switch(opt)
			{
			case 1:
				newGame();
				break;
			case 2:
				finish = true;
				break;
			}
			Screen.clear();
		}
	}

	/**
	 * Menú de selección de dificultad de una nueva partida de Sudoku
	 **/
	public void newGame()
	{
		sudoku.resetGame();
		in.nextLine();
		Screen.clear();

		System.out.println("Nivel de dificultad");

		System.out.println("1. Infantil (50)");
		System.out.println("2. Normal (40)");
		System.out.println("3. Dificil (34)");
		System.out.println("4. Maquiavelico (26)");
		System.out.println("5. Diabolico (17)");
		System.out.println("6. Regresar al Menu Principal");

		int opt = in.nextInt();

		switch(opt)
		{
		case 1:
			sudoku.setUpGame(Rating.INFANTIL);
			break;
		case 2:
			sudoku.setUpGame(Rating.NORMAL);
			break;
		case 3:
			sudoku.setUpGame(Rating.DIFICIL);
			break;
		case 4:
			sudoku.setUpGame(Rating.MAQUIAVELICO);
			break;
		case 5:
			sudoku.setUpGame(Rating.DIABOLICO);
			break;
		case 6:
			Screen.clear();
			mainMenu();
			break;

		}

		Screen.clear();
		startGame();

	}
	Rating[] difficultyLvl = Rating.values();

	/**
	 * TODO: Realice un menú de selección de dificultad, de acuerdo a los niveles
	 *       declarados y dispuestos en el arreglo difficultyLvl. Nota: Es posible
	 *       realizar la impresión de cada uno de los niveles, a partir de la invocación
	 *       de la función toString(). Adicionalmente, es necesario presentar una opción 
	 *       que permita al usuario retornar al menú principal. Para este fin, es suficiente
	 *       permitir que el método actual finalice sin ejecutar instrucciones adicionales
	 **/

	/**
	 * Ciclo principal de ejecución del juego de Sudoku
	 **/
	public void startGame()
	{
		while(!sudoku.hasGameFinished())
		{
			Screen.clear();
			System.out.println(printBoard(sudoku.getBoard()));

			System.out.println("Escoje una opcion");
			System.out.println("1. Cambiar valor de casilla y eliminar casilla");
			System.out.println("2. Ver solucion y seguir jugando");
			System.out.println("3. Ver solucion y salir al Menu Principal");

			int opt=in.nextInt();

			switch(opt)
			{
			case 1:
				System.out.println("Inserte fila (1 - 9)");
				int numFila = in.nextInt();
				System.out.println("Inserte columna (1 - 9)");
				int numColumna = in.nextInt();
				System.out.println("Inserte valor");
				int valor = in.nextInt();
				System.out.println("Desea borrar el valor de la casilla? Si(true); no(false)");
				String ansDelete = in.next();
				if(ansDelete.equalsIgnoreCase("Si"))
				{
					Screen.clear();
					sudoku.replaceValue(0, 0, 0, true);
					System.out.println(printBoard(sudoku.getBoard()));
				}
				else{
					Screen.clear();
					sudoku.replaceValue((numFila-1), (numColumna-1), valor,	false);
					System.out.println(printBoard(sudoku.getBoard()));}

				break;
			case 2:

				in.nextLine();
				Screen.clear();
				System.out.println("Solución posible al juego");
				System.out.println("-------------------------");
				System.out.println(printBoard(sudoku.getSolution()));

				System.out.print("Presione cualquier tecla para continuar...");
				in.nextLine();
			case 3:
				in.nextLine();
				Screen.clear();
				System.out.println("Solución posible al juego");
				System.out.println("-------------------------");
				System.out.println(printBoard(sudoku.getSolution()));

				System.out.print("Presione cualquier tecla para volver al menu principal...");
				in.nextLine();
				Screen.clear();
				mainMenu();
			}
		}
		/**
		 * TODO: Construya la interfaz de interacción principal con la partida en curso.
		 *       Esta interfaz debe, en primer lugar, visualizar el estado actual del
		 *       tablero de juego (Ver printBoard). En segundo lugar, debe ofrecer un menú
		 *       bajo el cual, el usuario pueda realizar las siguientes acciones:
		 *           - Introducir un valor en una casilla
		 *           - Eliminar el valor de una casilla
		 *           - Finalizar juego y ver solución
		 *       Finalmente, es necesario verificar el estado de finalización de juego
		 *       tras la ejecución de un cambio en el tablero (Ver Sudoku.hasGameFinished())
		 **/
	}

	/**
	 * Dialogo dispuesto para la sustitución de un valor en el tablero de sudoku.
	 * Si el usuario ingresa un valor incorrecto, o si el valor ingresado viola alguna
	 * de las restricciones del juego, se informará al mismo a través de consola.
	 **/
	public void enterValue()
	{
		in.nextLine();
		System.out.println("\nIntroduzca la coordenada de la casilla, acompañada del número a introducir, e.g., A2-3\n");
		System.out.print("Casilla: ");

		String coor = in.nextLine();
		String[] values = coor.split("-");
		int num = Integer.parseInt(values[1]);
		int row = ((int) values[0].charAt(0)) - 65;
		int col = Integer.parseInt(""+values[0].charAt(1))-1;
		if(row < 0 || row > 8)
		{
			System.out.println("La coordenada de la fila debe encontrarse en el rango A-I.");
		}
		else
		{
			if(col < 0 || col > 8)
			{
				System.out.println("La coordenada de la columna debe encontrarse en el rango 1-9.");
			}
			else
			{
				String result = sudoku.replaceValue(row, col, num, false);
				System.out.println("\n"+result);
			}
		}
		System.out.print("Presione cualquier tecla para continuar...");
		in.nextLine();
	}

	/**
	 * Dialogo dispuesto para la eliminación de un valor en el tablero de sudoku.
	 * Si el usuario ingresa un valor incorrecto, o si el valor ingresado viola alguna
	 * de las restricciones del juego, se informará al mismo a través de consola.
	 **/
	public void deleteValue()
	{
		in.nextLine();
		System.out.println("\nIntroduzca la coordenada de la casilla que se desea eliminar, e.g., B4\n");
		System.out.print("Casilla: ");

		String coor = in.nextLine();
		int row = ((int) coor.charAt(0)) - 65;
		int col = Integer.parseInt(""+coor.charAt(1))-1;
		if(row < 0 || row > 8)
		{
			System.out.println("La coordenada de la fila debe encontrarse en el rango A-I.");
		}
		else
		{
			if(col < 0 || col > 8)
			{
				System.out.println("La coordenada de la columna debe encontrarse en el rango 1-9.");
			}
			else
			{
				String result = sudoku.replaceValue(row, col, 0, true);
				System.out.println("\n"+result);
			}
		}
		System.out.print("Presione cualquier tecla para continuar...");
		in.nextLine();
	}

	/**
	 * Imprime un tablero de Sudoku en consola, a partir del uso de caractéres de construcción de tablas.
	 * @param Matriz que describe un tablero de un juego de Sudoku.
	 * Cada entrada de la matriz, contiene un número entre 1 y 9 si ésta no se
	 * encuentra vacía. 0 de lo contrario
	 * @return Representación textual de un tablero de sudoku.
	 **/
	public String printBoard(int[][] board)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\n     1   2   3   4   5   6   7   8   9\n");
		sb.append("   ┌───┬───┬───┬───┬───┬───┬───┬───┬───┐\n");
		int rowNum = 1;
		for(int[] row : board)
		{
			sb.append(" "+((char)(rowNum + 64))+" ");
			for(int col : row)
			{
				String val = (col == 0) ? " " : ""+col;
				sb.append("│ "+val+" ");
			}
			sb.append("│\n");
			if(rowNum != 9)
			{
				sb.append("   ├───┼───┼───┼───┼───┼───┼───┼───┼───┤\n");
			}
			else
			{
				sb.append("   └───┴───┴───┴───┴───┴───┴───┴───┴───┘\n");
			}
			rowNum++;
		}
		return sb.toString();
	}


}

